﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Data.ADO
{
    public class GetData
    {
        public string ConnectionString { get; private set; }
        public GetData(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void GetXML()
        {
            string qry = @"SELECT c.ContactId, c.Nom, c.Prenom
FROM Reference.ville v
JOIN Contact.Adresse a ON v.VilleId = a.VilleId
JOIN Contact.Contact c ON a.AdresseId = c.AdressePostaleId
WHERE v.NomVille = 'Paris'
FOR XML AUTO, ELEMENTS, ROOT('contacts');";

            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand(qry, cn))
                {
                    var rd = cmd.ExecuteXmlReader();
                    rd.Read();
                    rd.Close();
                }
                cn.Close();
            }


            //System.Xml.XmlReader reader = command.ExecuteXmlReader();
        }
    }
}
