﻿using System.Data.SqlClient;
using System.Data;

namespace PachaDataManager.Data.ADO
{
    public class PlanCache
    {
        public string ConnectionString { get; private set; }

        public PlanCache(string connectionString)
        {
            var cnb = new SqlConnectionStringBuilder(connectionString);
            cnb.MultipleActiveResultSets = true;
            this.ConnectionString = cnb.ConnectionString;
        }

        public void TrivialPlan(int iterations = 200)
        {
            int i = 1;
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmdLoop = new SqlCommand("SELECT ContactId FROM Contact.Contact WITH (READUNCOMMITTED);",cn))
                {
                    var rdLoop = cmdLoop.ExecuteReader();
                    
                    while (rdLoop.Read() && i <= iterations)
                    {
                        var contactId = rdLoop.GetInt32(0);
                        using (var cmd = new SqlCommand(string.Format("SELECT Nom FROM Contact.Contact WITH (READUNCOMMITTED) WHERE ContactId = {0};", contactId), cn))
                        {
                            var rs = cmd.ExecuteScalar();
                        }
                        i++;
                    }
                }
                cn.Close();
            }
        }

        public void NonTrivialPlan(int iterations = 200)
        {
            int i = 1;
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmdLoop = new SqlCommand("SELECT DISTINCT Nom, Prenom FROM Contact.Contact WITH (READUNCOMMITTED);", cn))
                {
                    var rdLoop = cmdLoop.ExecuteReader();

                    while (rdLoop.Read() && i <= iterations)
                    {
                        using (var cmd = new SqlCommand(
                            string.Format("SELECT * FROM Contact.Contact WITH (READUNCOMMITTED) WHERE Nom = '{0}' AND Prenom = '{1}';", 
                            rdLoop.GetString(0).Replace("'", "''"),
                            rdLoop.GetString(1).Replace("'", "''"))
                            , cn))
                        {
                            var rs = cmd.ExecuteScalar();
                        }
                        i++;
                    }
                }
                cn.Close();
            }
        }

        public void ParameterSniffing(int iterations = 200, bool doSeek = true)
        {
            int i = 1;
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmdLoop = new SqlCommand("SELECT DISTINCT Nom, Prenom FROM Contact.Contact WITH (READUNCOMMITTED);", cn))
                {
                    var rdLoop = cmdLoop.ExecuteReader();

                    (new SqlCommand("DBCC FREEPROCCACHE;", cn)).ExecuteNonQuery();

                    using (var cmd = new SqlCommand("Contact.GetContact", cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        var paramNom = cmd.Parameters.Add("@Nom", SqlDbType.VarChar, 50);

                        // premier appel
                        if (doSeek)
                            paramNom.Value = "Meguenni";
                        else
                            paramNom.Value = "Laurent";

                        var rd = cmd.ExecuteReader();
                        rd.Read();
                        rd.Close();

                        while (rdLoop.Read() && i <= iterations)
                        {
                            paramNom.Value = rdLoop.GetString(0);
                            rd = cmd.ExecuteReader();
                            rd.Read();
                            rd.Close();
                            i++;
                        }
                    }
                    cn.Close();
                }
            }
        }

    }
}
