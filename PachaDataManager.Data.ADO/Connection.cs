﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Data.ADO
{
    public enum DataProvider
    {
        Native,
        ODBC,
        OLEDB
    }

    public class Connection
    {
        public string ConnectionString { get; private set; }

        public Connection(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private void DoLoop(string connectionString, int iterations)
        {
            for (var i = 1; i < iterations; i++)
            {
                using (var cn = new SqlConnection(connectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand("SELECT TOP 1 * FROM Contact.Contact;", cn))
                    {
                        var res = cmd.ExecuteScalar();
                    }
                    cn.Close();
                }
            }
        }

        public void DoLoopWithPool(int iterations)
        {
            DoLoop(ConnectionString, iterations);
        }

        public void DoLoopWithoutPool(int iterations)
        {
            var cnb = new SqlConnectionStringBuilder(ConnectionString);
            cnb.Pooling = false;
            DoLoop(cnb.ConnectionString, iterations);
        }

        public void TestSingleRowOption(int iterations, bool SingleRowOption, bool Top1)
        {
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand(String.Format("SELECT {0} * FROM Contact.Contact;", Top1 ? "TOP 1": ""), cn))
                {
                    for (var i = 1; i < iterations; i++)
                    {
                        var rd = cmd.ExecuteReader(SingleRowOption ? System.Data.CommandBehavior.SingleRow : System.Data.CommandBehavior.Default);
                        rd.Read();
                        rd.Close();
                    }
                }
                cn.Close();
            }
        }

        public void TestExecuteScalar(int iterations, bool ExecuteScalar)
        {
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand("SELECT TOP 1 Nom FROM Contact.Contact;", cn))
                {
                    for (var i = 1; i < iterations; i++)
                    {
                        if (ExecuteScalar)
                        {
                            var r = cmd.ExecuteScalar();
                        }
                        else
                        {
                            var rd = cmd.ExecuteReader();
                            rd.Read();
                            rd.Close();
                        }
                    }
                }
                cn.Close();
            }
        }
    }
}
