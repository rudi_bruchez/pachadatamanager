﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Data.ADO
{
    public class Servant
    {
        public string ConnectionString { get; private set; }
        private SqlConnection _cn;

        public Servant(string connectionString)
        {
            ConnectionString = connectionString;
            _cn = new SqlConnection(ConnectionString);
            _cn.Open();
        }

        public SqlConnection Connection
        {
            get { return _cn; }
        }

        ~Servant()
        {
            if (_cn.State != System.Data.ConnectionState.Closed)
            {
                try
                {
                    _cn.Close();
                }
                finally
                {
                    _cn.Dispose();
                }
            }
        }

        public void Execute(string qry)
        {
            using (var cmd = new SqlCommand(qry, _cn))
            {
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
