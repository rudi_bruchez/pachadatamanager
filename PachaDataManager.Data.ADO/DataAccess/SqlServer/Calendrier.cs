﻿using System;
using System.Collections.Generic;
using DbParallel.DataAccess;

namespace PachaDataManager.Data.ADO.DataAccess.SqlServer
{
    public class SalleFormation
    {
        public int LieuFormationId { get; set; }
        public int SalleFormationId { get; set; }
        public string NomLieu { get; set; }
        public string Salle { get; set; }
        public string Ville { get; set; }
    }

    public class Session
    {
        public int SessionId { get; set; }
        public string Stage { get; set; }
        public int SalleFormationId { get; set; }
        public object DateDebut { get; set; }
        public object DateFin { get; set; }
    }

    public class Calendrier
    {
        private static string GetProcedure(string sp)
        {
            return ConfigHelper.DatabasePackage + sp;
        }

        public Tuple<List<SalleFormation>, List<Session>> GetData(DateTime date)
        {
            const string sp = "dbo.GetCalendrier";

            var resultTuple = Tuple.Create(new List<SalleFormation>(), new List<Session>());

            using (DbAccess db = DbPackage.CreateConnection())
            {
                db.ExecuteMultiReader(GetProcedure(sp), parameters =>
                {
                    parameters.Add("DateDebut", date);
                }, resultSets =>
                {
                    // Specified fields mapping example
                    resultSets.Add(resultTuple.Item1, colMap =>  // 1st ResultSet
                    {
                        colMap.Add("LieuFormationId", t => t.LieuFormationId);
                        colMap.Add("SalleFormationId", t => t.SalleFormationId);
                        colMap.Add("NomLieu", t => t.NomLieu);
                        colMap.Add("Salle", t => t.Salle);
                        colMap.Add("Ville", t => t.Ville);
                    });

                    // Full-automatic (case-insensitive) fields mapping examples
                    resultSets.Add(resultTuple.Item2);   // 2nd ResultSet
                }
                );
        }
            return resultTuple;
        }
    }
}
