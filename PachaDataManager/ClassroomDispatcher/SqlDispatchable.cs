﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.ClassroomDispatcher
{
    public abstract class SqlDispatchable
    {
        public string ConnectionString { get; private set; }
        public PachaDataManager.ViewModel.ShowMessage ShowMessage { protected get; set; }
        protected string Server
        {
            get
            {
                var cnb = new SqlConnectionStringBuilder(ConnectionString);
                return cnb.DataSource;
            }
        }

        protected SqlDispatchable(string connectionString)
        {
            ConnectionString = connectionString;
            var cnb = new SqlConnectionStringBuilder(connectionString);
            cnb.MultipleActiveResultSets = true;
            ConnectionString = cnb.ConnectionString;
        }

        public abstract void Execute(int iterations, int interval, byte etape = 1);
    }
}
