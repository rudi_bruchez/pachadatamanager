﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Diagnostic
{
    public class ContactAvecInscription
    {
        public string Nom;
        public string Prenom;
        public int Inscriptions;
    }

    public class LinqToSql
    {
        private string _connectionString;

        public LinqToSql(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<ContactAvecInscription> GetContactsAvecInscriptions()
        {
            var dc = new PachaDataManager.Data.LinqToSql.PachaDataFormationDataContext(_connectionString);
            var qry = from c in dc.Contact
                      let InscriptionCount = c.Inscription.Count()
                      orderby c.Nom
                      select new ContactAvecInscription { Nom = c.Nom, Prenom = c.Prenom, Inscriptions = InscriptionCount };
            return qry.ToList();
        }

        public List<ContactAvecInscription> GetContactsAvecInscriptionsModulaire()
        {
            var dc = new PachaDataManager.Data.LinqToSql.PachaDataFormationDataContext(_connectionString);

            var ins = from i in dc.Inscription
                      group i by i.ContactId into g
                      select new { ContactId = g.Key, cnt = g.Count() };

            var qry = from c in dc.Contact
                      let InscriptionCount = ins.Where(r => r.ContactId == c.ContactId).Select(r => r.cnt).SingleOrDefault()
                      orderby c.Nom
                      select new ContactAvecInscription { Nom = c.Nom, Prenom = c.Prenom, Inscriptions = InscriptionCount };
            return qry.ToList();
        }

        private int CountInscriptions(int ContactId)
        {
            var dc = new PachaDataManager.Data.LinqToSql.PachaDataFormationDataContext(_connectionString);

            return dc.Inscription.Count(r => r.ContactId == ContactId);
        }

        public List<ContactAvecInscription> GetContactsAvecInscriptionsFonctionnel()
        {
            var dc = new PachaDataManager.Data.LinqToSql.PachaDataFormationDataContext(_connectionString);

            var qry = from c in dc.Contact
                      orderby c.Nom
                      select new ContactAvecInscription { Nom = c.Nom, Prenom = c.Prenom, Inscriptions = CountInscriptions(c.ContactId) };
            return qry.ToList();
        }

    }
}
