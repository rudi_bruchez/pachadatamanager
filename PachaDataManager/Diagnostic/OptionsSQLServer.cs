﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Diagnostic
{
    public class OptionsSQLServer
    {
        private string _connectionString;
        private Data.ADO.Servant _servant;
        private ViewModel.ShowMessage _showMessage;

        public OptionsSQLServer(string connectionString, ViewModel.ShowMessage showmessage)
        {
            _connectionString = connectionString;
            _showMessage = showmessage;
        }

        /// <summary>
        /// Les effet de l'option AUTO_CLOSE sur une base de données
        /// </summary>
        public void AutoClose(int iterations, int sleepMilliseconds)
        {
            // TODO - enlever le pool de connexions
            string qry = @"
                USE Master; 
                ALTER DATABASE PachaDataFormation SET SINGLE_USER WITH ROLLBACK IMMEDIATE; 
                ALTER DATABASE PachaDataFormation SET AUTO_CLOSE ON WITH NO_WAIT;
                ALTER DATABASE PachaDataFormation SET MULTI_USER; 
            ";
            _servant.Execute(qry);

            // alimentation en boucle
            for (var i = 0; i <= iterations; i++)
            {
                //qry = String.Format("INSERT INTO dbo.message_edi (contenu) VALUES ('{0}');", Guid.NewGuid());
                _servant.Execute(qry);
            }
        }
    }
}
