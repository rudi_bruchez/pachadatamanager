﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace PachaDataManager.Diagnostic
{
    class Memoire
    {
        public string ConnectionString { private set; get; }

        public Memoire(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public void runMemoire(int iterations, int sleepMilliseconds, ViewModel.ShowMessage showmessage)
        {
            showmessage("démarrage de l'opération mémoire");
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                for (int i = 1; i <= iterations; i++)
                {
                    string sql = String.Format(@"
	                    DECLARE cur{0} CURSOR KEYSET
	                    FOR SELECT Nom FROM Contact.Contact;

	                    DECLARE @nom varchar(50)

	                    OPEN cur{0}
	                    FETCH NEXT FROM cur{0} INTO @nom
                    ", i.ToString());
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                    showmessage(String.Format("itération {0}", i));
                    System.Threading.Thread.Sleep(sleepMilliseconds);
                }
            }

            //Console.WriteLine("itérations terminées. La connexion reste ouverte. Appuyez sur une touche pour la fermer");
        }

    }
}
