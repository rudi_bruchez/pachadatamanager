﻿using System;
using System.Data.SqlClient;

namespace PachaDataManager.Diagnostic
{
    public class AsyncNetworkIo : ClassroomDispatcher.SqlDispatchable
    {
        //private string _connectionString { get; private set; }

        public AsyncNetworkIo(string connectionString)
        : base(connectionString)
        { }

        // merci d'éviter un nombre trop grand d'itérations
        // 1 itération = 1 thread

        public override void Execute(int iterations, int interval, byte etape = 1)
        {
            using (var cn = new SqlConnection(base.ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = System.Data.CommandType.Text;
                    //cmd.CommandText = "DBCC FREEPROCCACHE";
                    //cmd.ExecuteNonQuery();
                    cmd.CommandText = "SELECT DISTINCT * FROM Contact.Contact";
                    var rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        var nom = rd.GetInt32(0);
                        System.Threading.Thread.Sleep(interval);
                    }
                    cn.Close();
                    base.ShowMessage(String.Format("exécution de AsyncNetworkIo terminée sur serveur {0}", base.Server));
                }
            }
        }
    }
}
