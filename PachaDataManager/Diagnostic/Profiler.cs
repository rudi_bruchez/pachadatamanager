﻿using System;
using System.Data.SqlClient;

namespace PachaDataManager.Diagnostic
{
    public class Profiler : ClassroomDispatcher.SqlDispatchable
    {
        //private string _connectionString { get; private set; }

        public Profiler(string connectionString)
        : base(connectionString)
        { }

        // merci d'éviter un nombre trop grand d'itérations
        // 1 itération = 1 thread

        public override void Execute(int iterations, int interval, byte etape = 1)
        {
            int contactId;

            using (var cn = new SqlConnection(base.ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT DISTINCT Nom FROM Contact.Contact";
                    cmd.CommandType = System.Data.CommandType.Text;
                    var rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        var nom = rd.GetString(0);
                        using (var cmd2 = new SqlCommand())
                        {
                            cmd2.Connection = cn;
                            cmd2.CommandText = String.Format("SELECT * FROM Contact.Contact WHERE nom = '{0}';", nom.Replace("'", "''"));
                            cmd2.CommandType = System.Data.CommandType.Text;
                            cmd2.ExecuteReader();
                        }
                    }
                    cn.Close();
                }
            }
        }
    }
}
