﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.Optimisation
{
    public class Indexation
    {
        private Data.ADO.Servant _servant;

        public Indexation(string connectionString)
        {
            _servant = new Data.ADO.Servant(connectionString);
        }

        public void Requete01()
        {
            string qry = @"SELECT sl.Titre, COUNT(DISTINCT i.ContactId) as inscritsTotaux
FROM Inscription.Inscription i
JOIN Stage.Session se ON i.SessionId = se.SessionId
JOIN Stage.StageLangue sl ON sl.StageId = se.StageId AND sl.LangueCd = se.LangueCd
JOIN Stage.Stage s ON sl.StageId = s.StageId
WHERE sl.Titre LIKE 'Postgres%'
GROUP BY sl.Titre
ORDER BY inscritsTotaux DESC;";

            _servant.Execute(qry);
        }

        public void Requete02()
        {
            string qry = "SELECT * FROM Contact.Contact WHERE UPPER(Nom) = N'FERAGOTTO';";

            _servant.Execute(qry);
        }

        public void Requete03()
        {
            string qry = @"SELECT i.InscriptionId, i.DateCreation
FROM Stage.Session s
JOIN Inscription.Inscription i ON s.SessionId = i.SessionId
WHERE s.Statut = 'A'; ";

            _servant.Execute(qry);
        }

    }
}
