﻿using System.ComponentModel;
using System.Windows;

namespace PachaDataManager.SqlConnectionWindow
{
    /// <summary>
    /// code principalement emprunté à
    /// https://github.com/JakeGinnivan/SqlConnectionControl
    /// </summary>
    public partial class SqlConnectionWindow : Window
    {
        private SqlConnectionString _test;

        public SqlConnectionWindow()
        {
            InitializeComponent();
        }

        public SqlConnectionString Test
        {
            get { return _test; }
            set
            {
                _test = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Test"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
