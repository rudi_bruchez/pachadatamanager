﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity; // pour avoir les lambdas dans les objets EF
using System.IO;
using System.Linq;
using PachaDataManager.ViewModel;
using System.Threading.Tasks;
using PachaDataManager.Outils;
using System.Diagnostics;

/*
https://cgeers.wordpress.com/2011/05/19/entity-framework-bulk-copy/
https://efbulkinsert.codeplex.com/

You can call a stored procedure in your DbContext class as follows.
this.Database.SqlQuery<YourEntityType>("storedProcedureName",params);
But if your Stored Procedure return multiple result sets as your sample code, then you can see this helpful article on MSDN 
http://msdn.microsoft.com/en-us/data/jj691402.aspx

You can improve performance of many includes by creating 2 or more small data request from data base like below.

According to my experience,Only can give maximum 2 includes per query like below.More than that will give really bad performance.

var userData = from u in db.Users
                        .Include("UserSkills.Skill")
                        .Include("UserIdeas.IdeaThings")
                        .FirstOrDefault();

 userData = from u in db.Users
                    .Include("UserFriends.User.UserSkills.Skill")
                    .Include("UserFriends1.User1.UserSkills.Skill")
                    .FirstOrDefault();
TODO : créer un intercepteur
https://cmatskas.com/logging-and-tracing-with-entity-framework-6/
*/

namespace PachaDataManager.EntityFramework
{
    /// <summary>
    /// Une classe qui représente des appels EF vers l'entité contacts
    /// pour démonstration des effets des appels.
    /// </summary>
    /// les appels sont à tracer dans le profiler SQL Server pour voir les effets.
    class Contacts
    {
        //private Hermes _hermes;
        private ViewModel.ShowMessage _showMessage;

        public Contacts(ViewModel.ShowMessage showMessage)
        {
            _showMessage = showMessage;
        }


        /// <summary>
        /// Exemple de requête simple sur un index clustered
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public Data.EF.Contact GetContact(int contactId)
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                var qry = from c in cx.Contact
                          where c.ContactId == contactId
                          select c;
                
                /* utilisation de la méthode Single() pour "optimizer" (?)
                   le retour */
                return qry.Single();
            }
        }

        /// <summary>
        /// Exemple de recherche par nom, avec projection
        /// </summary>
        /// <param name="nom">Nom du contact à rechercher (exact)</param>
        public string GetContactProjection(string nom)
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                /* au lieu de retourner l'entité entière dans le select, on construit une classe
                   anonyme avec les membres qui nous intéressent 
                   => projection dans le SELECT généré.
                   => meilleure couverture par l'index, et génération d'un meilleur plan d'
                */
                var qry = from c in cx.Contact
                          where c.Nom == nom
                          select new { Nom = c.Nom, Prenom = c.Prenom, ContactId = c.ContactId };
                return qry.First().Prenom;
            }
        }


        /// <summary>
        /// Démonstration de l'effet du passage dans une fonction du côté gauche de l'opérande
        /// Effet pervers : conversion en Unicode par EF
        /// </summary>
        /// <param name="nom">Nom du contact à rechercher (exact)</param>
        /// <returns></returns>
        public string GetContactUpper(string nom)
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                var qry = from c in cx.Contact
                          where c.Nom.ToUpper() == nom
                          select new { Nom = c.Nom, Prenom = c.Prenom, ContactId = c.ContactId };
                return qry.First().Prenom;
            }
        }

        /// <summary>
        /// Démonstration d'une projection qui contient des membres
        /// de deux entités différentes
        /// </summary>
        /// <param name="nom">Nom du contact à rechercher (exact)</param>
        /// <returns></returns>
        public string GetContactProjectionConnexe(string nom)
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                var qry = from c in cx.Contact
                          where c.Nom == nom
                          select new {
                              Nom = c.Nom,
                              Prenom = c.Prenom,
                              ContactId = c.ContactId,
                              Societe = c.Societe.Nom // un membre de l'entité connexe Societe
                          };
                return qry.First().Societe;
            }
        }

        public Data.EF.Contact GetContactByName(string nom)
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                var qry = from c in cx.Contact
                          where c.Nom.Contains(nom)
                          select c;
                return qry.First();
            }
        }

        public void NoLazyLoading()
        {
            var lstSocietes = new List<string>();

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                /* -- ICI - tracer batchstrarting et batchcompleted pour montrer l'action 
                            du DataReader sous-jacent
                */
                cx.Database.Log = message => Trace.Write(message);

                cx.Configuration.LazyLoadingEnabled = false;
                var contacts = cx.Contact.Where(r => r.SocieteId != null).ToList();

                foreach (var c in contacts)
                {
                    //cx.entry(c).reference(s => s.societe).load();
                    // cx.Entry(c).Collection(s => s.Societes).Load(); // si relation 1-plusieurs
                    lstSocietes.Add(String.Format("{0} {1} ", c.Prenom, c.Nom));
                }
            }
        }

        public void ConnectedDisconnected()
        {
            var lstSocietes = new List<string>();

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                cx.Configuration.LazyLoadingEnabled = false;
                var contacts = cx.Contact.Where(r => r.SocieteId != null).ToList(); // dataset

                foreach (var c in contacts)
                {
                    //cx.entry(c).reference(s => s.societe).load();
                    // cx.Entry(c).Collection(s => s.Societes).Load(); // si relation 1-plusieurs
                    lstSocietes.Add(String.Format("{0} {1} ", c.Prenom, c.Nom));
                }
            }
        }


        public void LazyLoadingToList()
        {
            var lstSocietes = new List<string>();

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                cx.Configuration.LazyLoadingEnabled = true;
                var contacts = cx.Contact.Where(r => r.SocieteId != null).ToList();

                foreach (var c in contacts)
                {
                    //cx.Entry(c).Reference(s => s.Societe).Load();
                    // cx.Entry(c).Collection(s => s.Societes).Load(); // si relation 1-plusieurs
                    lstSocietes.Add(String.Format("{0} {1} de la société {2}", c.Prenom, c.Nom, c.Societe.Nom));
                }
            }
        }


        /// <summary>
        /// Désactivation de MultipleActiveResultset pour voir ce que ça donne en cas de LazyLoading
        /// </summary>
        public void WithoutMars()
        {
            // changer la chaîne de connexion
            var lstSocietes = new List<string>();

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                foreach (var c in cx.Contact.Where(r => r.SocieteId != null))
                {
                    lstSocietes.Add(String.Format("{0} {1} de la société {2}", c.Prenom, c.Nom, c.Societe.Nom));
                }
            }
        }

        public void Update()
        {
            var iterations = 20;

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);
                
                //cx.Configuration.AutoDetectChangesEnabled = false;
                //cx.Configuration.ValidateOnSaveEnabled = false;
                foreach (var c in cx.Contact.Take(iterations))
                {
                    c.Nom = "hello";
                }
                cx.SaveChanges(); // on peut toujours rêver
            }
        }

        public void IncludeFiltre()
        {
            var lstSocietes = new List<string>();

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                foreach (var c in cx.Contact
                .Include(c => c.Societe)
                .Where(r => r.Societe.Nom.StartsWith("C"))
                .Select(c => new { Nom = c.Nom, Prenom = c.Prenom, NomSociete = c.Societe.Nom }))
                {
                    lstSocietes.Add(String.Format("{0} {1} de la société {2}", c.Prenom, c.Nom, c.NomSociete));
                }
            }
        }

        public void RequeteN1(bool include = false)
        {
            var lstSocietes = new List<string>();

            _showMessage("Connexion au contexte...");
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                if (include)
                {
                    //_mainvm.
                    foreach (var c in cx.Contact
                        .Include(c => c.Societe)
                        .Where(r => r.SocieteId != null)
                        .Select(c => new { Nom = c.Nom, Prenom = c.Prenom, NomSociete = c.Societe.Nom }))
                    {
                        lstSocietes.Add(String.Format("{0} {1} de la société {2}", c.Prenom, c.Nom, c.NomSociete));
                    }
                }
                else
                {
                    foreach (var c in cx.Contact.Where(r => r.SocieteId != null))
                    {
                        lstSocietes.Add(String.Format("{0} {1} de la société {2}", c.Prenom, c.Nom, c.Societe.Nom));
                    }
                }
            }
        }

        internal void Insertions(string connectionString, string sourcePath)
        {
            if (!File.Exists(sourcePath))
            {
                throw new FileNotFoundException(string.Format("Le fichier source ({0}) n'existe pas", sourcePath));
            }
            //var import = new ADO.ImportProspects(connectionString, sourcePath, this);
            //var dt = import.dataTable;

            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                throw new NotImplementedException();
            }
        }

        internal void EntitySQL()
        {
            using (var cx = new Data.EF.PachaDataFormationEntities())
            {
                cx.Database.Log = message => Trace.Write(message);

                var oc = ((IObjectContextAdapter)cx).ObjectContext;
            }
        }
    }   
}
