﻿using PachaDataManager.ViewModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Threading.Tasks;
using System.Xml;

namespace PachaDataManager.ADO
{
    public class ImportProspects
    {
        private const string _table = "Contact.ProspectUS";

        public string ConnectionString { get; private set; }
        public string SourcePath { get; private set; }
        private ViewModel.ShowMessage _showMessage;

        public DataTable dataTable
        {
            get { return FillDataTable(); }
        }

        public ImportProspects(string connectionString, string sourcePath, ViewModel.ShowMessage showMessage)
        {
            var cnb = new SqlConnectionStringBuilder(connectionString);
            cnb.MultipleActiveResultSets = true;
            ConnectionString = cnb.ConnectionString;
            if (!File.Exists(sourcePath))
            {
                throw new FileNotFoundException(string.Format("Le fichier source ({0}) n'existe pas", sourcePath));
            }
            SourcePath = sourcePath;
            _showMessage = showMessage;
        }

        private void TruncateTable()
        {
            using (var cn = new SqlConnection(ConnectionString))
            {
                using (var cmd = new SqlCommand(string.Format("TRUNCATE TABLE {0};", _table), cn))
                {
                    cn.Open();
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
        }

        private DataTable FillDataTable()
        {
            var sr = new StreamReader(SourcePath);
            var line = sr.ReadLine();
            string[] value = line.Split(',');
            var dt = new DataTable(_table);
            DataRow row;
            foreach (string dc in value)
            {
                dt.Columns.Add(new DataColumn(dc));
            }

            while (!sr.EndOfStream)
            {
                value = sr.ReadLine().Split(',');
                if (value.Length == dt.Columns.Count)
                {
                    row = dt.NewRow();
                    row.ItemArray = value;
                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        public void ManualImport()
        {
            //_mainvm.Messages.CurrentThread
            _showMessage("démarrage de l'importation BCP");
            TruncateTable();

            using (var dt = FillDataTable())
            {
                using (var cn = new SqlConnection(ConnectionString))
                {
                    using (var cmd = new SqlCommand(
                        string.Format("INSERT INTO {0} (Prenom, Nom, Tel, Email) VALUES(@Prenom, @Nom, @Tel, @Email)", _table), cn))
                    {
                        var pPrenom = new SqlParameter("@Prenom", SqlDbType.VarChar, 50);
                        var pNom = new SqlParameter("@Nom", SqlDbType.VarChar, 50);
                        var pTel = new SqlParameter("@Tel", SqlDbType.VarChar, 50);
                        var pEmail = new SqlParameter("@Email", SqlDbType.VarChar, 50);
                        cmd.Parameters.Add(pPrenom);
                        cmd.Parameters.Add(pNom);
                        cmd.Parameters.Add(pTel);
                        cmd.Parameters.Add(pEmail);

                        cn.Open();

                        foreach (DataRow r in dt.Rows)
                        {
                            pPrenom.Value = r[0].ToString();
                            pNom.Value = r[1].ToString();
                            pTel.Value = r[2].ToString();
                            pEmail.Value = r[3].ToString();

                            cmd.ExecuteNonQuery();
                        }

                        cn.Close();
                    }
                }
            }
        }

        public void BCP()
        {
            _showMessage("démarrage de l'importation BCP");
            TruncateTable();
            
            using (var dt = FillDataTable())
            {
                using (var cn = new SqlConnection(ConnectionString))
                {
                    var bc = new SqlBulkCopy(cn.ConnectionString, SqlBulkCopyOptions.TableLock);

                    // copier seulement les colonnes désirées
                    bc.ColumnMappings.Add(new SqlBulkCopyColumnMapping(0, "Prenom"));
                    bc.ColumnMappings.Add(new SqlBulkCopyColumnMapping(1, "Nom"));
                    bc.ColumnMappings.Add(new SqlBulkCopyColumnMapping(2, "Tel"));
                    bc.ColumnMappings.Add(new SqlBulkCopyColumnMapping(3, "Email"));

                    bc.DestinationTableName = _table;
                    bc.BatchSize = dt.Rows.Count;
                    cn.Open();
                    bc.WriteToServer(dt);
                    bc.Close();
                    cn.Close();
                }
            }
        }

        public DataTable XML()
        {
            TruncateTable();

            var dt = new DataTable();

            using (var mst = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(mst, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    using (var dt1 = FillDataTable())
                    {
                        dt1.WriteXml(writer);
                    }

                    //serializer.Serialize(writer, myList.ToArray(), ns);

                    using (var cn = new SqlConnection(ConnectionString))
                    {
                        using (var cmd = new SqlCommand("Contact.InsertProspect_XML", cn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            var param = new SqlParameter("@XMLInput", SqlDbType.Xml)
                            {
                                Value = new SqlXml(mst)
                            };
                            cmd.Parameters.Add(param);
                            cn.Open();
                            //var rd = cmd.ExecuteReader();
                            //Messagebox.show
                            
                            dt.Load(cmd.ExecuteReader());
                        }
                        cn.Close();
                    }
                }
            }
            return dt;
        }
    }
}
