﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PachaDataManager.Data.ADO;

namespace PachaDataManager
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string _version = "1.2";
        private ViewModel.Main vm;
        public string ConnectionString { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            this.Title = "PachaDataManager v." + _version;
            vm = new ViewModel.Main();
            DataContext = vm;
        }

        private void tviDiag_Memoire_Selected(object sender, RoutedEventArgs e)
        {
            vm.Diag_Memoire_Run();
        }

        private void tviEF_GetContacts_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_GetContacts();
        }

        private void tviADO_Connection_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Connection(DataProvider.Native);
            //vm.ADO_Connection(DataProvider.ODBC);
            vm.ADO_Connection(DataProvider.OLEDB);
        }

        private void tviADO_DataReader_SansAffichage_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Reader_SansAffichage();
        }

        private void tviADO_DataReader_AvecAffichage_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Reader_AvecAffichage();
        }

        private void tviADO_DataReader_DataBooster_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_DataBooster_sproc();
        }

        private void tviDiag_GetContactsAvecInscriptions_Selected(object sender, RoutedEventArgs e)
        {
            vm.Diag_Contacts_Avec_Inscriptions();
        }

        private void tviDiag_GetContactsAvecInscriptionsModulaire_Selected(object sender, RoutedEventArgs e)
        {
            vm.Diag_Contacts_Avec_Inscriptions_Modulaire();
        }

        private void tviDiag_GetContactsAvecInscriptionsFonctionnel_Selected(object sender, RoutedEventArgs e)
        {
            vm.Diag_Contacts_Avec_Inscriptions_Fonctionnel();
        }

        private void tviADO_Connection_SansPool_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Connection_SansPool();
        }

        private void tviADO_Connection_AvecPool_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Connection_AvecPool();
        }

        private void tviADO_Command_TestSingleRowOption_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Command_TestSingleRowOption();
        }

        private void tviADO_Command_TestExecuteScalar_Selected(object sender, RoutedEventArgs e)
        {
            vm.ADO_Command_TestExecuteReader();
        }

        private void tviPlanCache_TrivialPlan_Selected(object sender, RoutedEventArgs e)
        {
            vm.PlanCache_TrivialPlan();
        }

        private void tviPlanCache_NonTrivialPlan_Selected(object sender, RoutedEventArgs e)
        {
            vm.PlanCache_NonTrivialPlan();
        }

        private void tviPlanCache_Proc_ParameterSniffing_Selected(object sender, RoutedEventArgs e)
        {
            vm.PlanCache_Proc_ParameterSniffing();
        }

        private void tviImports_Manual_Selected(object sender, RoutedEventArgs e)
        {
            vm.Import_Manual();
        }

        private void tviImports_BCP_Selected(object sender, RoutedEventArgs e)
        {
            vm.Import_BCP();
        }

        private void tviImports_XML_Selected(object sender, RoutedEventArgs e)
        {
            RecordsetDataGrid.DataContext = vm.Import_XML().DefaultView;
        }

        private void tviEF_RequeteN1_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_RequeteN1();
        }

        private void tviEF_RequeteN1_Include_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_RequeteN1_Include();
        }

        private void tviEF_Filtrage_Like_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_Filtrage_Like();
        }

        private void tviEF_Projection_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_GetContacts_Projection();
        }

        private void tviEF_ProjectionConnexe_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_GetContacts_ProjectionConnexe();
        }

        private void tviTypique_EDI_Selected(object sender, RoutedEventArgs e)
        {
            vm.Typique_EDI();
        }

        private void tviOptimisation_Indexation_Requetes_Selected(object sender, RoutedEventArgs e)
        {
            vm.Optimisation_Indexation_Requetes();
        }

        private void tviEF_Insertions_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_Insertions();
        }

        private void tviEF_EntitySQL_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_EntitySQL();
        }

        private void tviEF_GetContactsToUpper_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_GetContacts_Upper();
        }

        private void tviEF_NoLazyLoading_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_NoLazyLoading();
        }

        private void tviEF_IncludeFiltre_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_IncludeFiltre();
        }

        private void tviEF_Update_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_Update();
        }

        private void tviEF_WithoutMars_Selected(object sender, RoutedEventArgs e)
        {
            vm.EF_WithoutMars();
        }

        private void btnDispatcher_Click(object sender, RoutedEventArgs e)
        {
            vm.Dispatcher_Run();
        }

        private void tviDispatcher_Attentes_Selected(object sender, RoutedEventArgs e)
        {
            vm.Dispatcher_Attentes();
        }

        private void tviDispatcher_AdHoc_Selected(object sender, RoutedEventArgs e)
        {
            vm.Dispatcher_AdHoc();
        }

        private void tviDispatcher_EDI_Selected(object sender, RoutedEventArgs e)
        {
            vm.Dispatcher_Edi();
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            vm.SetConnection();
        }
    }
}
