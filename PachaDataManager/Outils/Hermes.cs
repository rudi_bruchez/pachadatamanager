﻿using PachaDataManager.ViewModel;
using System.Threading;
using System.Threading.Tasks;

namespace PachaDataManager.Outils
{

    /// <summary>
    /// Classe de communication entre le thread de travail et le thread UI.
    /// La communication se fait principalement avec le ViewModel, on passe
    /// donc une référence vers le TaskScheduler du thread princiapl (UI)
    /// et une référence sur l'instance du ViewModel.
    /// </summary>
    public class Hermes
    {
        private Main _mainvm;
        private TaskScheduler _maints;

        public Hermes(Main mainvm, TaskScheduler maints)
        {
            _mainvm = mainvm;
            _maints = maints;
        }

        public void SendMessage(string message)
        {
            //Task.Factory.StartNew(() =>
            //{
            //    _mainvm.Messages.Add(message);
            //}, CancellationToken.None, TaskCreationOptions.None, _maints);
        }
    }
}
