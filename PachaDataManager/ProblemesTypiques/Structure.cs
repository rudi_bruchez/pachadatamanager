﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PachaDataManager.ProblemesTypiques
{
    public class Structure
    {
        private Data.ADO.Servant _servant;

        public Structure(string connectionString)
        {
            _servant = new Data.ADO.Servant(connectionString);
        }

        /// <summary>
        /// Simulation d'un import dans une application EDI.
        /// Effets à tracer avec le profiler du côté serveur pour juger de l'importance d'une bonne structure
        /// </summary>
        /// <param name="iterations"></param>
        public void EDI(int iterations = 100000)
        {
            // création de la table
            var qry = @"IF OBJECT_ID(N'dbo.message_edi', N'U') IS NOT NULL DROP TABLE dbo.message_edi;";
            _servant.Execute(qry);

            qry = @"CREATE TABLE dbo.message_edi (
                        message_id UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT(NEWID()),
	                    date_message datetime2(2) NOT NULL DEFAULT(SYSDATETIME()),
                        contenu text
                        );";
            _servant.Execute(qry);

            // première alimentation
            qry = @"INSERT INTO dbo.message_edi
                        (contenu)
                        SELECT text
                        FROM master.sys.messages v1
                        CROSS JOIN (VALUES(1), (2)) v2(c);";
            _servant.Execute(qry);

            // alimentation en boucle
            for (var i = 0; i <= iterations; i++)
            {
                qry = String.Format("INSERT INTO dbo.message_edi (contenu) VALUES ('{0}');", Guid.NewGuid());
                _servant.Execute(qry);
            }
         }
    }
}
