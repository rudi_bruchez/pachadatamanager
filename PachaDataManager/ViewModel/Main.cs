﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using PachaDataManager.Data.ADO;
using System.Windows.Controls;
using NLog;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using PachaDataManager.Extensions;

namespace PachaDataManager.ViewModel
{
    public delegate void ShowMessage(string message);

    public class Main: INotifyPropertyChanged
    {
        private string _importFilePath;
        //private static Logger _logger = LogManager.GetLogger("PachaDataManager");
        private ShowMessage _showmessage;
        private Dictionary<string, string> _options;

        private TaskScheduler _maints
        {
            get
            {
                return TaskScheduler.FromCurrentSynchronizationContext();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public SqlConnectionStringBuilder ConnectionStringBuilder { get; private set; }
        public string ServerName
        {
            get
            {
                return ConnectionStringBuilder.DataSource;
            }
            set
            {
                ConnectionStringBuilder.DataSource = ServerName;
            }
        }
        //private ObservableCollection<string> Messages { get; set; }
        private SynchronizedObservableCollection<string> Messages { get; set; }

        // pour alimenter l'ObservableCollection depuis un background thread
        private object _messagesLock = new object();

        public Main()
        {
            ConnectionStringBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["PachaDataFormation"].ConnectionString);
            _showmessage = new ShowMessage(AfficheMessage);
            _options = new Dictionary<string, string>();
            Messages = new SynchronizedObservableCollection<string>();

            // pour alimenter l'ObservableCollection depuis un background thread
            BindingOperations.EnableCollectionSynchronization(Messages, _messagesLock);
        }

        #region méthodes privées

        private Dictionary<string, string> GetOptions(UserControl ctrl)
        {
            var w = new Options.OptionWindow();
            return new Dictionary<string, string>();
        }

        private bool SetImportFilePath()
        {
            if (! String.IsNullOrWhiteSpace(_importFilePath))
                return true;

            var ofd = new Microsoft.Win32.OpenFileDialog();
            if (ofd.ShowDialog() == true)
            { 
                _importFilePath = ofd.FileName;
                return true;
            }
            else
            {
               //throw new Exception("Vous devez indiquer un fichier");
               return false;
            }
        }

        #endregion

        internal async void EF_GetContacts()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            var res = await Task.Run(() => c.GetContact(1));
        }

        #region méthodes ADO

        internal void ADO_Connection(DataProvider dp)
        {
            Messages.Add("Connexion avec le provider ");
            System.Data.Common.DbConnection cn;
            string st;
            var appName = ConnectionStringBuilder.ApplicationName;

            switch (dp)
            { 
                case DataProvider.Native:
                    ConnectionStringBuilder.ApplicationName = appName + " Native";
                    cn = new SqlConnection(ConnectionStringBuilder.ConnectionString);
                    break;
                case DataProvider.ODBC:
                    st = String.Format("Driver={{SQL Server Native Client 11.0}};Server={0};Database={1};{2};APP={3};",
                        ConnectionStringBuilder.DataSource,
                        ConnectionStringBuilder.InitialCatalog,
                        ConnectionStringBuilder.IntegratedSecurity ? "Trusted_Connection=yes" :
                            String.Format("uid = {0}; pwd = {1}", ConnectionStringBuilder.UserID, ConnectionStringBuilder.Password),
                        appName + " ODBC"
                        );

                    cn = new System.Data.Odbc.OdbcConnection(st);
                    break;
                case DataProvider.OLEDB:
                    st = String.Format("Provider=SQLNCLI11;Server={0};Database={1};{2};Application Name={3};",
                        ConnectionStringBuilder.DataSource,
                        ConnectionStringBuilder.InitialCatalog,
                        ConnectionStringBuilder.IntegratedSecurity ? "Trusted_Connection=yes" : 
                            String.Format("Uid={0};Pwd={1}", ConnectionStringBuilder.UserID, ConnectionStringBuilder.Password),
                        appName + " OLEDB"
                        );

                    cn = new System.Data.OleDb.OleDbConnection(st);
                    break;
                default:
                    cn = new SqlConnection(ConnectionStringBuilder.ConnectionString);
                    break;
            }

            cn.Open();
            var cmd = cn.CreateCommand();
            cmd.CommandText = "SELECT * FROM Contact.Contact;";
            var rd = cmd.ExecuteReader();
            rd.Close();
            cn.Close();
            ConnectionStringBuilder.ApplicationName = appName;
        }

        internal void Optimisation_Indexation_Requetes()
        {
            var idx = new Optimisation.Indexation(ConnectionStringBuilder.ConnectionString);
            idx.Requete01();
            idx.Requete02();
            idx.Requete03();
        }

        internal void EF_Insertions()
        {
            if (SetImportFilePath())
            {
                var c = new EntityFramework.Contacts(AfficheMessage);
                c.Insertions(ConnectionStringBuilder.ConnectionString, _importFilePath);
            }
        }

        internal void EF_WithoutMars()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.WithoutMars();
        }

        internal void Dispatcher_Run()
        {
            var d = new ClassroomDispatcher.SqlDispatcher(10, 100, AfficheMessage);
            d.GetServerList();
            d.ExecuteAsync<Diagnostic.AsyncNetworkIo>(ConnectionStringBuilder.ConnectionString);
        }

        internal void Dispatcher_AdHoc()
        {
            var d = new ClassroomDispatcher.SqlDispatcher(10, 100, AfficheMessage);
            d.GetServerList();
            d.ExecuteAsync<Diagnostic.Profiler>(ConnectionStringBuilder.ConnectionString);
        }

        internal void SetConnection()
        {
            var cnw = new SqlConnectionWindow.SqlConnectionWindow();
            cnw.ShowDialog();
        }

        internal void Dispatcher_Edi()
        {
            //var d = new ClassroomDispatcher.SqlDispatcher(5000, 100, AfficheMessage, 2);
            var d = new ClassroomDispatcher.SqlDispatcher(5000, 100, _showmessage, 2);
            d.GetServerList();
            d.ExecuteAsync<ClassroomDispatcher.Dispatchable.Edi>(ConnectionStringBuilder.ConnectionString);
        }

        internal void Dispatcher_Attentes()
        {
            var d = new ClassroomDispatcher.SqlDispatcher(10, 100, AfficheMessage);
            d.GetServerList();
            d.ExecuteAsync<Diagnostic.AsyncNetworkIo>(ConnectionStringBuilder.ConnectionString);
        }

        internal void EF_GetContacts_Upper()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.GetContactUpper("FERAGOTTO");
        }

        internal void EF_NoLazyLoading()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.NoLazyLoading();
        }

        internal void EF_IncludeFiltre()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.IncludeFiltre();
        }

        internal void EF_Update()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.Update();
        }

        internal void EF_EntitySQL()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.EntitySQL();
        }

        internal void Typique_EDI()
        {
            var typique = new ProblemesTypiques.Structure(ConnectionStringBuilder.ConnectionString);
            typique.EDI();
        }

        internal void EF_GetContacts_Projection()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.GetContactProjection("Simon");
        }

        internal void EF_GetContacts_ProjectionConnexe()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.GetContactProjectionConnexe("Simon");
        }

        internal void EF_Filtrage_Like()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.GetContactByName("ferra");
        }

        internal void EF_RequeteN1()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            Messages.Add("Exécution de RequeteN1");
            Task.Factory.StartNew(() => c.RequeteN1(false));
            //Messages.Add("Exécution de RequeteN1 terminée");
        }

        internal void EF_RequeteN1_Include()
        {
            var c = new EntityFramework.Contacts(AfficheMessage);
            c.RequeteN1(true);
        }

        #endregion
        #region méthodes importation

        internal DataTable Import_XML()
        {
            if (SetImportFilePath())
            {
                var imp = new ADO.ImportProspects(ConnectionStringBuilder.ConnectionString, _importFilePath, AfficheMessage);
                return imp.XML();
            }
            else
            {
                return null;
            }
        }

        internal void Import_BCP()
        {
            if (SetImportFilePath())
            {
                var imp = new ADO.ImportProspects(ConnectionStringBuilder.ConnectionString, _importFilePath, AfficheMessage);
                imp.BCP();
            }
        }

        internal async void Import_Manual()
        {
            if (SetImportFilePath())
            { 
                var imp = new ADO.ImportProspects(ConnectionStringBuilder.ConnectionString, _importFilePath, AfficheMessage);
                await Task.Run(() => imp.ManualImport());
            }
        }
        
        #endregion

        internal void PlanCache_Proc_ParameterSniffing()
        {
            var pc = new Data.ADO.PlanCache(ConnectionStringBuilder.ConnectionString);
            pc.ParameterSniffing();
        }

        internal void PlanCache_NonTrivialPlan()
        {
            var pc = new Data.ADO.PlanCache(ConnectionStringBuilder.ConnectionString);
            pc.NonTrivialPlan();
        }

        internal void PlanCache_TrivialPlan()
        {
            var pc = new Data.ADO.PlanCache(ConnectionStringBuilder.ConnectionString);
            pc.TrivialPlan();
        }

        internal void ADO_Command_TestExecuteReader()
        {
            var cn = new Data.ADO.Connection(ConnectionStringBuilder.ConnectionString);
            cn.TestExecuteScalar(200, false);
            cn.TestExecuteScalar(200, true);
        }

        internal void ADO_Command_TestSingleRowOption()
        {
            var cn = new Data.ADO.Connection(ConnectionStringBuilder.ConnectionString);
            //_logger.Trace("start TestSingleRowOption False");
            cn.TestSingleRowOption(200, false, true);
            //_logger.Trace("stop TestSingleRowOption False");
            //_logger.Trace("start TestSingleRowOption True");
            cn.TestSingleRowOption(200, true, true);
            //_logger.Trace("stop TestSingleRowOption True");
        }

        internal void ADO_Connection_SansPool()
        {
            var cn = new Data.ADO.Connection(ConnectionStringBuilder.ConnectionString);
            cn.DoLoopWithoutPool(200);
        }

        internal void ADO_Connection_AvecPool()
        {
            var cn = new Data.ADO.Connection(ConnectionStringBuilder.ConnectionString);
            cn.DoLoopWithPool(200);
        }
        internal void Diag_Contacts_Avec_Inscriptions_Modulaire()
        {
            var cc = new Diagnostic.LinqToSql(ConnectionStringBuilder.ConnectionString);

            Messages.Clear();
            foreach (var c in cc.GetContactsAvecInscriptionsModulaire())
            {
                Messages.Add(String.Format("{0} {1} a eu {2} inscriptions", c.Prenom, c.Nom.ToUpper(), c.Inscriptions));
            }
        }

        internal void Diag_Contacts_Avec_Inscriptions_Fonctionnel()
        {
            var cc = new Diagnostic.LinqToSql(ConnectionStringBuilder.ConnectionString);

            Messages.Clear();
            foreach (var c in cc.GetContactsAvecInscriptionsFonctionnel())
            {
                Messages.Add(String.Format("{0} {1} a eu {2} inscriptions", c.Prenom, c.Nom.ToUpper(), c.Inscriptions));
            }
        }

        internal void Diag_Contacts_Avec_Inscriptions()
        {
            var cc = new Diagnostic.LinqToSql(ConnectionStringBuilder.ConnectionString);

            Messages.Clear();
            foreach (var c in cc.GetContactsAvecInscriptions())
            {
                Messages.Add(String.Format("{0} {1} a eu {2} inscriptions", c.Prenom, c.Nom.ToUpper(), c.Inscriptions));
            }
        }

        internal void ADO_DataBooster_sproc()
        {
            var cal = new Data.ADO.DataAccess.SqlServer.Calendrier();
            cal.GetData(DateTime.Today);
        }

        internal void ADO_Reader_SansAffichage()
        {
            using (var cn = new SqlConnection(ConnectionStringBuilder.ConnectionString))
            {
                cn.Open();
                var cmd = cn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Contact.Contact;";
                var rd = cmd.ExecuteReader();
                rd.Close();
                cn.Close();
            }
        }

        internal void ADO_Reader_AvecAffichage()
        {
            using (var cn = new SqlConnection(ConnectionStringBuilder.ConnectionString))
            {
                cn.Open();
                var cmd = cn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Contact.Contact;";
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    Messages.Add(rd[0].ToString());
                    OnPropertyChanged("Messages");
                }
                rd.Close();
                cn.Close();
            }
        }

        internal async void Diag_Memoire_Run()
        {
            var mem = new Diagnostic.Memoire(ConnectionStringBuilder.ConnectionString);
            //var showmessage = new ShowMessage(AfficheMessage);
            Messages.Clear();
            await Task.Run(() => mem.runMemoire(1000, 500, _showmessage));
            //bg.RunWorkerAsync(o => mem.runMemoire(1000, 500, _showmessage));
            //ThreadPool.QueueUserWorkItem(o => );
        }

        private void AfficheMessage(string message)
        {
             Messages.Add(message);
        }
    }
}
