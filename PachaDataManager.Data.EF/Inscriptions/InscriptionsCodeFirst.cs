namespace PachaDataManager.Data.EF.Inscriptions
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class InscriptionsCodeFirst : DbContext
    {
        public InscriptionsCodeFirst()
            : base("name=InscriptionsCodeFirst")
        {
        }

        public virtual DbSet<AdresseSurInscription> AdresseSurInscription { get; set; }
        public virtual DbSet<Evaluation> Evaluation { get; set; }
        public virtual DbSet<Facture> Facture { get; set; }
        public virtual DbSet<Inscription> Inscription { get; set; }
        public virtual DbSet<SuiviFacture> SuiviFacture { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Evaluation>()
                .Property(e => e.Observations)
                .IsUnicode(false);

            modelBuilder.Entity<Evaluation>()
                .Property(e => e.Ajouter)
                .IsUnicode(false);

            modelBuilder.Entity<Evaluation>()
                .Property(e => e.Supprimer)
                .IsUnicode(false);

            modelBuilder.Entity<Evaluation>()
                .Property(e => e.Formation)
                .IsUnicode(false);

            modelBuilder.Entity<Evaluation>()
                .Property(e => e.Moyenne)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Facture>()
                .Property(e => e.FactureCd)
                .IsUnicode(false);

            modelBuilder.Entity<Facture>()
                .Property(e => e.CodeRemise)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Facture>()
                .Property(e => e.Remise)
                .HasPrecision(10, 7);

            modelBuilder.Entity<Facture>()
                .Property(e => e.PART)
                .HasPrecision(7, 4);

            modelBuilder.Entity<Facture>()
                .Property(e => e.ReferenceCommande)
                .IsUnicode(false);

            modelBuilder.Entity<Facture>()
                .Property(e => e.MontantHT)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Facture>()
                .Property(e => e.MontantTTC)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Facture>()
                .Property(e => e.TauxTVA)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Inscription>()
                .Property(e => e.ReferenceCommande)
                .IsUnicode(false);

            modelBuilder.Entity<Inscription>()
                .Property(e => e.FeuilleEmargement)
                .IsUnicode(false);

            modelBuilder.Entity<Inscription>()
                .HasMany(e => e.AdresseSurInscription)
                .WithRequired(e => e.Inscription)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inscription>()
                .HasMany(e => e.Facture)
                .WithMany(e => e.Inscription)
                .Map(m => m.ToTable("InscriptionFacture", "Inscription").MapLeftKey("InscriptionId").MapRightKey("FactureCd"));

            modelBuilder.Entity<SuiviFacture>()
                .Property(e => e.FactureCd)
                .IsUnicode(false);

            modelBuilder.Entity<SuiviFacture>()
                .Property(e => e.TypePaiement)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<SuiviFacture>()
                .Property(e => e.NoChequeBanque)
                .IsUnicode(false);

            modelBuilder.Entity<SuiviFacture>()
                .Property(e => e.NoBordereau)
                .IsUnicode(false);

            modelBuilder.Entity<SuiviFacture>()
                .Property(e => e.Montant)
                .HasPrecision(8, 2);
        }
    }
}
