namespace PachaDataManager.Data.EF.Inscriptions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inscription.Inscription")]
    public partial class Inscription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inscription()
        {
            AdresseSurInscription = new HashSet<AdresseSurInscription>();
            Facture = new HashSet<Facture>();
        }

        public int InscriptionId { get; set; }

        [Index("nix_Inscription_SessionId")] // on ne peut pas faire d'include ...
        public int SessionId { get; set; }

        public int? DecideurInscriptionId { get; set; }

        public int? ContactId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateAnnulation { get; set; }

        public byte Remise { get; set; }

        public bool Present { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DateCreation { get; set; }

        [StringLength(100)]
        public string ReferenceCommande { get; set; }

        public bool ConventionEnvoyee { get; set; }

        public bool ConvocationEnvoyee { get; set; }

        public bool ListeAttente { get; set; }

        [StringLength(1000)]
        public string FeuilleEmargement { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdresseSurInscription> AdresseSurInscription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Facture> Facture { get; set; }
    }
}
