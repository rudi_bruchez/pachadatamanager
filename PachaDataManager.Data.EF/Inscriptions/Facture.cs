namespace PachaDataManager.Data.EF.Inscriptions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inscription.Facture")]
    public partial class Facture
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Facture()
        {
            SuiviFacture = new HashSet<SuiviFacture>();
            Inscription = new HashSet<Inscription>();
        }

        [Key]
        [StringLength(50)]
        public string FactureCd { get; set; }

        [StringLength(2)]
        public string CodeRemise { get; set; }

        public decimal? Remise { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DateCreation { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateFacture { get; set; }

        public byte Relance { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateRelance { get; set; }

        public decimal? PART { get; set; }

        [StringLength(100)]
        public string ReferenceCommande { get; set; }

        public decimal MontantHT { get; set; }

        public decimal MontantTTC { get; set; }

        public decimal TauxTVA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SuiviFacture> SuiviFacture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inscription> Inscription { get; set; }
    }
}
