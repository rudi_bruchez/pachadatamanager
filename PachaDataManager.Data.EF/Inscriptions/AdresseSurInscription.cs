namespace PachaDataManager.Data.EF.Inscriptions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inscription.AdresseSurInscription")]
    public partial class AdresseSurInscription
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InscriptionId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AdresseId { get; set; }

        public bool Facturation { get; set; }

        public bool Convention { get; set; }

        public bool LettreConvention { get; set; }

        public bool Attestation { get; set; }

        public bool Copie { get; set; }

        public virtual Inscription Inscription { get; set; }
    }
}
