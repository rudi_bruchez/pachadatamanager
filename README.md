# PachaDataManager #

Client C3 / WPF pour la base de données d'exemple PachaDataFormation

### De quoi s'agit-il ? ###

Client générique pour mes formations SQL Server
http://www.babaluga.com/contact

L'interface est en WPF.
Démonstration d'appels à SQL Server en ADO.NET, différents data providers, Entity Framework, ainsi que des exemples d'appels SQL ou de patterns d'accès qui posent des problèmes de performance ou qui permettent de développer des exercices de diagnotic.

Le backup de la base de données d'exemple est disponible dans les [téléchargements du dépôt](https://bitbucket.org/rudi_bruchez/pachadatamanager/downloads).

### Comment faire ? ###

* Restaurez la base de données d'exemple PachaDataFormation disponible dans les [téléchargements du dépôt](https://bitbucket.org/rudi_bruchez/pachadatamanager/downloads) dans une instance SQL Server 2012 minimum.
* Ouvrez la solution dans Visual Studio 2015, j'utilise l'édition communautaire. Ou prenez une version zippée de l'exécutable dans les [téléchargements du dépôt](https://bitbucket.org/rudi_bruchez/pachadatamanager/downloads). Ce n'est pas forcément la dernière version compilée du code disponible dans ce dépôt.
* Changez les chaînes de connexion dans le fichier App.Config du projet principal (PachaDataManager).
* good to go